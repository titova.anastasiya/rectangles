package org.titova.rectangles;

public class Rectangle {

    private final Integer ax;
    private final Integer ay;
    private final Integer bx;
    private final Integer by;

    public Rectangle(Integer ax, Integer ay, Integer bx, Integer by) {
        this.ax = ax;
        this.ay = ay;
        this.bx = bx;
        this.by = by;
    }

    public Integer getAx() {
        return ax;
    }

    public Integer getAy() {
        return ay;
    }

    public Integer getBx() {
        return bx;
    }

    public Integer getBy() {
        return by;
    }
}
