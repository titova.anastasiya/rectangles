package org.titova.rectangles;

import java.util.Scanner;


public class Application {
    public static void main(String[] args) {

        int ax;
        int ay;
        int bx;
        int by;
        Scanner scanner = new Scanner(System.in);

        System.out.println("Enter ax1");
        ax = scanner.nextInt();
        System.out.println("Enter ay1");
        ay = scanner.nextInt();
        System.out.println("Enter bx1");
        bx = scanner.nextInt();
        System.out.println("Enter by1");
        by = scanner.nextInt();
        Rectangle rectangle1 = new Rectangle(ax, ay, bx, by);

        System.out.println("Enter ax2");
        ax = scanner.nextInt();
        System.out.println("Enter ay2");
        ay = scanner.nextInt();
        System.out.println("Enter bx2");
        bx = scanner.nextInt();
        System.out.println("Enter by2");
        by = scanner.nextInt();
        Rectangle rectangle2 = new Rectangle(ax, ay, bx, by);

        System.out.println(intersection(rectangle1, rectangle2));

    }

    public static String intersection(Rectangle rectangle1, Rectangle rectangle2){
        if ((rectangle1.getAx() > rectangle2.getBx()) || (rectangle2.getAx() > rectangle1.getBx()) ||
                (rectangle1.getAy() > rectangle2.getBy()) || (rectangle2.getAy() > rectangle1.getBy())){
            return "Rectangles don't intersect";
        }

        if (((rectangle1.getAx().equals(rectangle2.getBx())) && (rectangle1.getAy().equals(rectangle2.getBy()))) ||
                ((rectangle2.getAx().equals(rectangle1.getBx())) && (rectangle2.getAy().equals(rectangle1.getBy()))) ||
                ((rectangle1.getAx().equals(rectangle2.getBx())) && (rectangle1.getBy().equals(rectangle2.getAy()))) ||
                ((rectangle1.getBx().equals(rectangle2.getAx())) && (rectangle1.getAy().equals(rectangle2.getBy())))){
            return "Point intersection";
        }

        if (((rectangle2.getAx() < rectangle1.getBx()) && (rectangle1.getAx() < rectangle2.getBx()) && (rectangle2.getAy().equals(rectangle1.getBy()))) ||
                ((rectangle1.getAx() < rectangle2.getBx()) && (rectangle2.getAx() < rectangle1.getBx()) && (rectangle1.getAy().equals(rectangle2.getBy())))){
            return "Horizontal line intersection";
        }

        if (((rectangle2.getAy() < rectangle1.getBy()) && (rectangle1.getAy() < rectangle2.getBy()) && (rectangle2.getAx().equals(rectangle1.getBx()))) ||
                ((rectangle1.getAy() < rectangle2.getBy()) && (rectangle2.getAy() < rectangle1.getBy()) && (rectangle1.getAx().equals(rectangle2.getBx())))){
            return  "Vertical line intersection";
        }

        if ((rectangle1.getBx() > rectangle2.getAx()) && (rectangle2.getBx() > rectangle1.getAx()) &&
                (rectangle1.getBy() > rectangle2.getAy()) && (rectangle2.getBy() > rectangle1.getAy())){
            return "Rectangle intersection";
        }
        return null;
    }
}
