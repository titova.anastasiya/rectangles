import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.titova.rectangles.Application;
import org.titova.rectangles.Rectangle;

import static org.junit.jupiter.api.Assertions.*;

public class ApplicationTest {

    Rectangle rectangle = new Rectangle(5,5,7,7);

    @Test
    public void testNoIntersection() {

        Rectangle rectangle1 = new Rectangle(8,5,9,7);
        Rectangle rectangle2 = new Rectangle(0,5,4,7);
        Rectangle rectangle3 = new Rectangle(5,8,7,9);
        Rectangle rectangle4 = new Rectangle(5,0,7,4);

        assertAll(
                () -> assertEquals("Rectangles don't intersect", Application.intersection(rectangle, rectangle1)),
                () -> assertEquals("Rectangles don't intersect", Application.intersection(rectangle, rectangle2)),
                () -> assertEquals("Rectangles don't intersect", Application.intersection(rectangle, rectangle3)),
                () -> assertEquals("Rectangles don't intersect", Application.intersection(rectangle, rectangle4))

        );
    }

    @Test
    public void testPointIntersection() {

        Rectangle rectangle1 = new Rectangle(7, 7, 8, 8);
        Rectangle rectangle2 = new Rectangle(3, 3, 5, 5);
        Rectangle rectangle3 = new Rectangle(7, 0, 8, 5);
        Rectangle rectangle4 = new Rectangle(3, 7, 5, 8);

        assertAll(
                () -> assertEquals("Point intersection", Application.intersection(rectangle, rectangle1)),
                () -> assertEquals("Point intersection", Application.intersection(rectangle, rectangle2)),
                () -> assertEquals("Point intersection", Application.intersection(rectangle, rectangle3)),
                () -> assertEquals("Point intersection", Application.intersection(rectangle, rectangle4))

        );
    }

    @Test
    public void testVerticalLineIntersection() {

        Rectangle rectangle1 = new Rectangle(7, 5, 8, 8);
        Rectangle rectangle2 = new Rectangle(3, 3, 5, 6);

        assertAll(
                () -> assertEquals("Vertical line intersection", Application.intersection(rectangle, rectangle1)),
                () -> assertEquals("Vertical line intersection", Application.intersection(rectangle, rectangle2))

        );
    }

    @Test
    public void testHorizontalLineIntersection() {

        Rectangle rectangle1 = new Rectangle(3, 7, 6, 9);
        Rectangle rectangle2 = new Rectangle(3, 3, 6, 5);

        assertAll(
                () -> assertEquals("Horizontal line intersection", Application.intersection(rectangle, rectangle1)),
                () -> assertEquals("Horizontal line intersection", Application.intersection(rectangle, rectangle2))

        );
    }

    @Test
    public void testRectangleIntersection() {

        Rectangle rectangle1 = new Rectangle(4, 4, 9, 9);

        assertAll(
                () -> assertEquals("Rectangle intersection", Application.intersection(rectangle, rectangle1))

        );
    }
}
